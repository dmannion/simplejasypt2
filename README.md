#### How to incorporate Jasypt into your Spring project:


##### Modify pom.xml for jasypt
```
    <properties>
        .
        .
        .
        <jasypt.version>1.9.2</jasypt.version>
    </properties>

    <dependencies>
        .
        .
        .
        <dependency>
            <groupId>org.jasypt</groupId>
            <artifactId>jasypt-spring31</artifactId>
            <version>${jasypt.version}</version>
        </dependency>
    </dependencies>

```

##### This next section is just for fun and demo.  In Real Life, Ops will perform this and provide the encrypted database password to you.
###### Optional: download [jasypt](http://www.jasypt.org/download.html), uncompress and run:
```
$ unzip jasypt-1.9.2-dist.zip

$ bin/encrypt.sh input='doublesupersecret' password='ma$ter pa$$word' algorithm='PBEWITHMD5ANDDES'

----ENVIRONMENT-----------------

Runtime: Oracle Corporation Java HotSpot(TM) 64-Bit Server VM 24.80-b11 



----ARGUMENTS-------------------

algorithm: PBEWITHMD5ANDDES
input: doublesupersecret
password: ma$ter pa$$word



----OUTPUT----------------------

3+p0mnafVBb5ld9eV7pD6mkFcPUXvaQxD6tuzIHxCiA=
```

##### Ops will give you an encrypted database password for your properties file.
* In this example, your encrypted database password is '3+p0mnafVBb5ld9eV7pD6mkFcPUXvaQxD6tuzIHxCiA='
* You get a real one from Ops and put it in your database properties file as `db.password=ENC(3+p0mnafVBb5ld9eV7pD6mkFcPUXvaQxD6tuzIHxCiA=)`
* Note that you no longer need to see the actual database password.
* Ops will set a transient environment variable with the decrypting password (only lives during application startup): 


```
$ export APP_ENCRYPTION_PASSWORD='ma$ter pa$$word'
```

##### Replace plain-text database password with encrypted database password:
```
mysql> create database simple;
Query OK, 1 row affected (0.00 sec)
```

`$ vim src/main/resources/db.properties`
```
#MySQL Options
#db.password=doublesupersecret // Delete this - you will no longer use the real plaintext password here
db.password=ENC(3+p0mnafVBb5ld9eV7pD6mkFcPUXvaQxD6tuzIHxCiA=)
db.username=simple
db.url=jdbc:mysql://localhost/simple
db.dialect=org.hibernate.dialect.MySQL5Dialect
db.driver=com.mysql.jdbc.Driver
```

##### Configure spring beans:

`$ vim src/main/webapp/WEB-INF/spring/db.xml`

```
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:mvc="http://www.springframework.org/schema/mvc"
    xmlns:context="http://www.springframework.org/schema/context"
    xmlns:tx="http://www.springframework.org/schema/tx"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
        http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc-3.0.xsd  
        http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.0.xsd
        http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd" default-autowire="byName">

<!-- PropertyPlaceholderConfigurer will be replaced by Jasypt's EncryptablePropertyPlaceholderConfigurer

    <bean id="placeholderConfig"
        class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="location" value="classpath:db.properties" />
    </bean>
-->

    <bean id="entityManagerFactory"
        class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
        <property name="jpaVendorAdapter">
            <bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter">
                <property name="showSql" value="true" />
                <property name="generateDdl" value="true" />
                <property name="databasePlatform" value="${db.dialect}" />
            </bean>
        </property>     
    </bean>

    <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource"
        destroy-method="close">
        <property name="driverClassName" value="${db.driver}" />
        <property name="url" value="${db.url}" />
        <property name="username" value="${db.username}" />
        <property name="password" value="${db.password}" />
    </bean>

    <bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">

    </bean>
        
    <bean class="org.springframework.orm.jpa.support.PersistenceAnnotationBeanPostProcessor" />
    
    <!-- jasypt config -->

    <!--                                                                      -->
    <!-- Configuration for encryptor, based on environment variables.         -->
    <!--                                                                      -->
    <!-- In this example, the encryption password will be read from an        -->
    <!-- environment variable called "APP_ENCRYPTION_PASSWORD" which, once    --> 
    <!-- the application has been started, could be safely unset.             -->
    <!--                                                                      -->
    <bean id="environmentVariablesConfiguration"
        class="org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig">
        <property name="algorithm" value="PBEWithMD5AndDES" />
        <property name="passwordEnvName" value="APP_ENCRYPTION_PASSWORD" />
    </bean>
  
  
    <!--                                                                      -->
    <!-- The will be the encryptor used for decrypting configuration values.  -->
    <!--                                                                      -->
    <bean id="configurationEncryptor"
            class="org.jasypt.encryption.pbe.StandardPBEStringEncryptor">
        <property name="config" ref="environmentVariablesConfiguration" />
    </bean>


    <!--                                                                      -->
    <!-- The EncryptablePropertyPlaceholderConfigurer will read the           -->
    <!-- .properties files and make their values accessible as ${var}.        -->
    <!--                                                                      -->
    <!-- Our "configurationEncryptor" bean (which implements                  --> 
    <!-- org.jasypt.encryption.StringEncryptor) is set as a constructor arg.  -->
    <!--                                                                      -->
    <bean id="propertyConfigurer"
        class="org.jasypt.spring31.properties.EncryptablePropertyPlaceholderConfigurer">
        <constructor-arg ref="configurationEncryptor" />
        <property name="locations">
            <list>
                <!-- <value>/WEB-INF/classes/db.properties</value> -->
                <value>classpath:db.properties</value>
            </list>
        </property>
   
    </bean>
</beans>
```

##### Clone this project, set up an empty mysql database schema and try it
```
$ git clone git@bitbucket.org:dmannion/simplejasypt2.git
$ mvn test tomcat7:run
```


Point browser to http://127.0.0.1:8080/


##### This spring-mvc-jpa-demo-archetype project was created from a maven archtype:

```
$ mvn archetype:generate -DgroupId=edu.ucop.app -DartifactId=SimpleJasypt2 -Dversion=0.1-SNAPSHOT -DarchetypeArtifactId=spring-mvc-jpa-demo-archetype -DarchetypeGroupId=org.fluttercode.knappsack -DinteractiveMode=false
```